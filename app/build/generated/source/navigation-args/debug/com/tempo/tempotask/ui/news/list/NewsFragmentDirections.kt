package com.tempo.tempotask.ui.news.list

import android.os.Bundle
import android.os.Parcelable
import androidx.navigation.NavDirections
import com.tempo.tempotask.R
import com.tempo.tempotask.data.models.news.Article
import java.io.Serializable
import java.lang.UnsupportedOperationException
import kotlin.Int
import kotlin.Suppress

class NewsFragmentDirections private constructor() {
  private data class ActionNewsFragmentToDetailsFragment(
    val article: Article
  ) : NavDirections {
    override fun getActionId(): Int = R.id.action_newsFragment_to_detailsFragment

    @Suppress("CAST_NEVER_SUCCEEDS")
    override fun getArguments(): Bundle {
      val result = Bundle()
      if (Parcelable::class.java.isAssignableFrom(Article::class.java)) {
        result.putParcelable("article", this.article as Parcelable)
      } else if (Serializable::class.java.isAssignableFrom(Article::class.java)) {
        result.putSerializable("article", this.article as Serializable)
      } else {
        throw UnsupportedOperationException(Article::class.java.name +
            " must implement Parcelable or Serializable or must be an Enum.")
      }
      return result
    }
  }

  companion object {
    fun actionNewsFragmentToDetailsFragment(article: Article): NavDirections =
        ActionNewsFragmentToDetailsFragment(article)
  }
}
