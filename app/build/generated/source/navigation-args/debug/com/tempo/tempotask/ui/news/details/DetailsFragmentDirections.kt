package com.tempo.tempotask.ui.news.details

import androidx.navigation.ActionOnlyNavDirections
import androidx.navigation.NavDirections
import com.tempo.tempotask.R

class DetailsFragmentDirections private constructor() {
  companion object {
    fun actionDetailsFragmentToNewsFragment(): NavDirections =
        ActionOnlyNavDirections(R.id.action_detailsFragment_to_newsFragment)
  }
}
