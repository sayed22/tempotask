package com.tempo.tempotask.data.api.impl

import retrofit2.HttpException
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.net.UnknownHostException


fun Throwable?.toErrorBody(): String {
    var s = when (this) {
        is SocketTimeoutException -> " Check Your Network Connection , Try Again later "
        is ConnectException -> " Check Your Network Connection , Try Again later "
        is UnknownHostException -> " Check Your Network Connection , Try Again later "
        is HttpException -> this.message()
        else -> this?.message
    }
    if (!s.isNullOrBlank()) {
        s = "Something Went Wrong"
    }
    return s.toString()
}








