package com.tempo.tempotask.data.api.news

import com.tempo.tempotask.data.api.impl.ApisConstants
import com.tempo.tempotask.data.models.news.NewsResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.QueryMap

interface NewsApi {

    @GET(ApisConstants.ALL_API)
    suspend fun getAll(@QueryMap queries: Map<String, String>): NewsResponse
}