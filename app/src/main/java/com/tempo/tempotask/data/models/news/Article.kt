package com.tempo.tempotask.data.models.news

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Article(
    val author: String="",
    val content: String="",
    val description: String="",
    val publishedAt: String="",
    val source: Source?=null,
    val title: String="",
    val url: String="",
    val urlToImage: String="",
    var type: Int = 0

):Parcelable