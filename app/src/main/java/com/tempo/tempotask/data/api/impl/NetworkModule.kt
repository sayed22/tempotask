package com.tempo.tempotask.data.api.impl

import com.google.gson.GsonBuilder
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

private val sLogLevel =
    HttpLoggingInterceptor.Level.BASIC

private const val baseUrl =
    ApisConstants.BASE_URL

private fun getLogInterceptor() = HttpLoggingInterceptor().apply { level = sLogLevel }

fun createNetworkClient(token: String? = null) =
    retrofitClient(baseUrl, okHttpClient(token, true))

private fun okHttpClient(token: String?, addAuthHeader: Boolean): OkHttpClient {
    return if (token.isNullOrEmpty()) {
        OkHttpClient.Builder()
            .addInterceptor(getLogInterceptor()).apply { setTimeOutToOkHttpClient(this) }
            .addInterceptor(ErrorLoggingInterceptor())
            .addInterceptor(headersInterceptor(addAuthHeader = addAuthHeader)).build()

    } else {
        OkHttpClient.Builder()
            .addInterceptor(getLogInterceptor()).apply { setTimeOutToOkHttpClient(this) }
            .addInterceptor(ErrorLoggingInterceptor())
            .addInterceptor(AuthenticationInterceptor(token))
            .addInterceptor(headersInterceptor(addAuthHeader = addAuthHeader)).build()
    }
}

private fun retrofitClient(baseUrl: String, httpClient: OkHttpClient): Retrofit =
    Retrofit.Builder()
        .baseUrl(baseUrl)
        .client(httpClient)
        .addConverterFactory(GsonConverterFactory.create(GsonBuilder().setLenient().create()))
        .build()


fun headersInterceptor(addAuthHeader: Boolean) = Interceptor { chain ->
    chain.proceed(
        chain.request().newBuilder()
            .addHeader("Content-Type", "application/json")
            .addHeader("Accept", "application/json")
            .build()
    )
}

private fun setTimeOutToOkHttpClient(okHttpClientBuilder: OkHttpClient.Builder) =
    okHttpClientBuilder.apply {
        readTimeout(30L, TimeUnit.SECONDS)
        connectTimeout(30L, TimeUnit.SECONDS)
        writeTimeout(30L, TimeUnit.SECONDS)
    }