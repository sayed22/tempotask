package com.tempo.tempotask.data.api.impl

import android.util.Log
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response

class ErrorLoggingInterceptor() : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val request: Request = chain.request()
        val response = chain.proceed(request)
        when (response.code) {
            400 -> {
                Log.i(javaClass.simpleName, "unauthrized")
            }
            401 -> {
                //Show UnauthorizedError Message
                Log.i(javaClass.simpleName, "bad Request")
            }
            403 -> {
                Log.i(javaClass.simpleName, "bad Request")
                //Show Forbidden Message
            }
            404 -> {
                //Show NotFound Message
                Log.i(javaClass.simpleName, "not found")
            }
            // ... and so on
        }
        return response
    }
}