package com.tempo.tempotask.data.models


enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}
