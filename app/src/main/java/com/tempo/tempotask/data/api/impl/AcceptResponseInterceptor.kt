package com.tempo.tempotask.data.api.impl

import okhttp3.Interceptor
import okhttp3.Response

class AcceptResponseInterceptor(
    val token: String = ""
) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {

        val original = chain?.request()
        return if (token.isNotEmpty()) {
            val builder = original?.newBuilder()
                ?.header("Authorization", "Bearer $token")
                ?.addHeader("Accept", "application/json")
                ?.addHeader("Content-Type", "application/json")
            chain!!.proceed(builder?.build())
        } else {
            val builder = original?.newBuilder()
                ?.addHeader("Accept", "application/json")
                ?.addHeader("Content-Type", "application/json")

            chain!!.proceed(builder?.build())
        }
    }
}