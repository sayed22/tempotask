package com.tempo.tempotask.data.repos

import com.tempo.tempotask.data.api.news.NewsApi

class NewsRepo(var newsApi: NewsApi) {

    suspend fun getAllNews(queryMap: Map<String, String>) = newsApi.getAll(queryMap)
}