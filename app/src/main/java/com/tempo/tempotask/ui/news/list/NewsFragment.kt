package com.tempo.tempotask.ui.news.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.tempo.tempotask.R
import com.tempo.tempotask.data.models.Status
import com.tempo.tempotask.data.models.news.Article
import com.tempo.tempotask.ui.news.NewsViewModel
import com.tempo.tempotask.utils.PaginationScrollListener
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_news.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.koin.android.viewmodel.ext.android.viewModel
import java.util.*

class NewsFragment : Fragment(), SwipeRefreshLayout.OnRefreshListener {
    private val activityScope = CoroutineScope(Dispatchers.Main)

    private val newsViewModel: NewsViewModel by viewModel()
    private var isLoading: Boolean = false
    private var isLastPage = false
    var articlesAdapter: ArticlesAdapter? = null
    private var fragmentView: View? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (fragmentView == null) {
            fragmentView = inflater.inflate(R.layout.fragment_news, container, false)
        }
        return fragmentView!!
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        //check to prevent fragment recreation with nav compo
        if (!newsViewModel.hasData)
            getNews()
        setUpPagination()
         setSearchListener()
        lay_refresh.setOnRefreshListener(this)
    }


    private fun getNews() {
        newsViewModel.getNews().observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.LOADING -> {
                    lay_refresh.isRefreshing = true
                }
                Status.SUCCESS -> {
                    newsViewModel.hasData = true
                    lay_refresh.isRefreshing = false
                    it.data?.let { data ->
                        if (data.articles.isNotEmpty()) {
                            lay_refresh.visibility = View.VISIBLE
                            tv_empty.visibility = View.GONE
                            newsViewModel.totalArticles = it.data!!.totalResults
                            addOrdersAdapter(data.articles)
                        } else {
                            lay_refresh.visibility = View.GONE
                            tv_empty.visibility = View.VISIBLE
                        }
                    }
                }
                Status.ERROR -> {
                    lay_refresh.isRefreshing = false
                    Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT).show()
                }
            }
        })
    }

    private fun addOrdersAdapter(articles: List<Article>) {
        if (articlesAdapter == null) {
            articlesAdapter =
                ArticlesAdapter(
                    articles as ArrayList<Article>,
                    this::onArticleClick
                )
            rv_news.adapter = articlesAdapter

        } else {
            if (isLoading) {
                articlesAdapter!!.removeLoadingFooter()
                isLoading = false
                articlesAdapter!!.add(articles as ArrayList<Article>)
            } else {
                articlesAdapter!!.updateList(articles as ArrayList<Article>)
            }
        }
    }

    private fun setUpPagination() {
        rv_news.addOnScrollListener(object :
            PaginationScrollListener(rv_news.layoutManager as LinearLayoutManager) {

            override fun isLastPage(): Boolean {
                return isLastPage
            }

            override fun isLoading(): Boolean {
                return isLoading
            }

            override fun loadMoreItems() {
                newsViewModel.currentPage = newsViewModel.currentPage.plus(1)
                if (newsViewModel.currentPage != (newsViewModel.totalArticles / 20)) {
                    isLoading = true
                    articlesAdapter!!.addLoadingFooter()
                    getNews()
                } else
                    isLastPage = true
            }


        })
    }


    private fun onArticleClick(article: Article) {
        val action = NewsFragmentDirections.actionNewsFragmentToDetailsFragment(article)
        NavHostFragment.findNavController(navigation_main)
            .navigate(action)
    }

    fun resetPagination() {
        isLoading = false
        isLastPage = false
        articlesAdapter = null
        newsViewModel.currentPage = 1
    }

    override fun onRefresh() {
        resetPagination()
        getNews()
    }

    private fun setSearchListener() {
        et_search.setOnQueryTextListener(object :
            androidx.appcompat.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextChange(newText: String): Boolean {
                if (newText.length >= 2) {
                    activityScope.launch {
                        delay(1000)
                        lay_refresh.isRefreshing = true
                        newsViewModel.searchText = newText
                        resetPagination()
                        getNews()
                    }
                    return true
                }
                return true

            }

            override fun onQueryTextSubmit(query: String): Boolean {
                return false
            }

        })
    }
}