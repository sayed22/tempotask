package com.tempo.tempotask.ui.news

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.tempo.tempotask.data.api.impl.ApisConstants
import com.tempo.tempotask.data.api.impl.toErrorBody
import com.tempo.tempotask.data.models.Resource
import com.tempo.tempotask.data.repos.NewsRepo
import kotlinx.coroutines.Dispatchers

class NewsViewModel(private val newsRepo: NewsRepo) : ViewModel() {

    var currentPage: Int = 1
    var totalArticles=0
    var searchText="apple"
    //flag to prevent fragment recreation
    var hasData:Boolean = false
    fun getNews() = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(newsRepo.getAllNews(setUpApiQueryMap(currentPage.toString(),searchText))))
        } catch (e: Throwable) {
            emit(Resource.error(data = null, msg = e.toErrorBody()))
            e.printStackTrace()
        }
    }


    private fun setUpApiQueryMap(page: String,searchText:String): Map<String, String> {
        var query = HashMap<String, String>()
        query["q"] = searchText
        query["from"] = "2020-08-12"
        query["sortBy"] = "publishedAt"
        query["apiKey"] = ApisConstants.API_KEY
        query["page"] = page

        return query
    }
}