package com.tempo.tempotask.ui.news.list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.tempo.tempotask.R
import com.tempo.tempotask.data.models.news.Article
import com.tempo.tempotask.utils.PaginationRecyclerViewAdapter
import kotlinx.android.synthetic.main.item_news.view.*
import kotlinx.android.synthetic.main.item_progress.view.*
import java.util.*

class ArticlesAdapter(
    private var list: ArrayList<Article>,
    val onClickListener: (  order: Article) -> Unit

) : PaginationRecyclerViewAdapter<Article>(R.layout.item_news, list) {


    override fun getItemCount(): Int = list.size


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        var viewHolder: RecyclerView.ViewHolder? = null
        val inflater = LayoutInflater.from(parent.context)

        when (viewType) {
            ITEM -> {
                val itemView = inflater.inflate(R.layout.item_news, parent, false)
                viewHolder = ListViewHolder(itemView)
            }

            LOADING -> {
                val loadingView = inflater.inflate(R.layout.item_progress, parent, false)
                viewHolder = LoadingViewHolder(loadingView)
            }

        }

        return viewHolder!!
    }


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (getItemViewType(position)) {
            ITEM -> {
                list[position].let {
                    holder.itemView.tv_article_title.text = it.title
                    Picasso.get().load(it.urlToImage).into(holder.itemView.iv_article)
                }
            }
            LOADING -> {
                holder.itemView.loadmore_progress.visibility = View.VISIBLE
            }

        }
    }

    override fun getItemViewType(position: Int): Int {

        return if (list[position].type == 0) {
            ITEM
        } else {
            LOADING
        }
    }

    fun updateList(notificationRow: ArrayList<Article>) {
        list = notificationRow
        notifyDataSetChanged()
    }

    fun add(notificationRow: ArrayList<Article>) {
        val lastPosition = itemCount
        list.addAll(notificationRow)
        notifyItemRangeInserted(lastPosition, itemCount)
    }


    fun remove(position: Int) {
        list.removeAt(position)
        notifyItemRemoved(position)
    }


    fun addLoadingFooter() {
        isLoadingAdded = true
        add(Article(type = 1))
    }

    inner class ListViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        init {
            view.setOnClickListener {
                onClickListener.invoke(  list[adapterPosition])

            }
        }
    }

    companion object {
        private const val ITEM = 0
        private const val LOADING = 1
    }
}