package com.tempo.tempotask.ui.news.details

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.squareup.picasso.Picasso
import com.tempo.tempotask.R
import com.tempo.tempotask.data.models.news.Article
import kotlinx.android.synthetic.main.fragment_details.*

class DetailsFragment : Fragment(R.layout.fragment_details) {

    private val args: DetailsFragmentArgs by navArgs()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val article = args.article
        setupDetails(article)
    }

    private fun setupDetails(article: Article) {
        Picasso.get().load(article.urlToImage).into(iv_image)
        tv_author.text = article.author
        tv_date.text = article.publishedAt
        tv_title.text = article.title
        tv_desc.text = article.description
        tv_content.text = article.content

        btn_source.setOnClickListener {
            val browserIntent =
                Intent(Intent.ACTION_VIEW, Uri.parse(article.url))
            startActivity(browserIntent)

        }
    }



}