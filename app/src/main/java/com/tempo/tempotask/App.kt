package com.tempo.tempotask

import android.app.Application
import com.tempo.tempotask.di.networkModule
import com.tempo.tempotask.di.reposModels
import com.tempo.tempotask.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@App)
            androidLogger()
            modules(listOf(networkModule, reposModels, viewModelModule))
        }
    }
}