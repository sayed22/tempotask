package com.tempo.tempotask.di

import com.tempo.tempotask.data.api.impl.createNetworkClient
import com.tempo.tempotask.data.api.news.NewsApi
import org.koin.dsl.module
import retrofit2.Retrofit


val retrofit: Retrofit = createNetworkClient()
private val MAIN_API: NewsApi = retrofit.create(NewsApi::class.java)
//private val DETAILS_API: DetailsApi = retrofit.create(DetailsApi::class.java)


val networkModule = module {
    single { MAIN_API }
//    single { DETAILS_API }

}