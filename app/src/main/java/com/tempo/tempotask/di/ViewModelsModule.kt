package com.tempo.tempotask.di

import com.tempo.tempotask.ui.news.NewsViewModel
import org.koin.dsl.module


val viewModelModule = module {
    single {
        NewsViewModel(get())
    }
}