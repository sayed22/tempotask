package com.tempo.tempotask.di

import com.tempo.tempotask.data.repos.NewsRepo
import org.koin.dsl.module

val reposModels = module {
    single {
        NewsRepo(get())
    }
}