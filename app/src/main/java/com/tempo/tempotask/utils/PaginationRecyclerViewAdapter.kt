package com.tempo.tempotask.utils

 import android.view.View
import android.view.ViewGroup
 import androidx.recyclerview.widget.RecyclerView

open class PaginationRecyclerViewAdapter<Item>(val LayoutId: Int, val data: MutableList<Item>)
    : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


    var isLoadingAdded = false
    public var retryPageLoad = false

    public var errorMsg: String? = null

    override fun getItemCount(): Int {
        return data.size
    }


    override fun getItemViewType(position: Int): Int {
        if (position == data!!.size - 1 && isLoadingAdded)
            return LOADING
        else
            return ITEM
    }


    fun add(item: Item) {
        data!!.add(item)
        notifyItemInserted(data!!.size - 1)
    }

    fun addAll(items: MutableList<Item>) {
        val oldSize: Int = itemCount
        this.data.addAll(items)
        notifyItemRangeInserted(oldSize, itemCount)
    }

    fun remove(item: Item?) {
        val position = data!!.indexOf(item)
        if (position > -1) {
            data!!.removeAt(position)
            notifyItemRemoved(position)
        }
    }

    fun removeLoadingFooter() {
        isLoadingAdded = false
        val position = data!!.size - 1
        data!!.removeAt(position)
        notifyItemRemoved(position)
    }

    fun clear() {
        val size = data.size
        data.clear()
        notifyItemRangeRemoved(0, size)
    }

    companion object {
        //view types
        val ITEM = 0
        val LOADING = 1
    }

    open class DataViewHolder(view: View) : RecyclerView.ViewHolder(view)

    inner class LoadingViewHolder(view: View) : RecyclerView.ViewHolder(view)
}