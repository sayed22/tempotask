Installation

Clone this repository and import into Android Studio

git clone https://Gohary00@bitbucket.org/Gohary00/tempotask.git

______________________________________________

The app is a stateless client: all operations/mutations are performed
by calling api endpoints over the network.

This app using MVVM architecture pattern

The domain model objects are used throughout the app. They are kotlin data classes.The network layer translates to and from the
local domain model as needed, the rest of the app should not have to ,know about those implementation details.

Tha repos layer contain the communication between the data source and our app asynchronously using kotlin coroutines in background thread. 

Activities and fragments are for presentation logic only. Each activity or fragment should have its own ViewModel where business 
logic is placed. The ViewModel reacts to data changes via the Live Data, and tells the fragment/activity how to update itself.


______________________________________________

External dependencies

Kotlin coroutines for async tasks 
Retrofit and Okhttp for networking 
Koin for dependency injection
Android architecture components (LiveData,ViewModel, Navigation component)
Picasso for image loading
